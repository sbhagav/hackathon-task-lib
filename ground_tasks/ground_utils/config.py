from secret import get_credentials
from quindar_tdk.core.context import GroundTaskContext

class ApiSettings:
    def __init__(self, task_context: GroundTaskContext):
        self.api_key, self.base_url = get_credentials(task_context)
        self.sat_id = "sbw3"
