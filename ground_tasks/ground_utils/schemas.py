from datetime import datetime
from typing import Any, Literal, Optional, Union, List
from uuid import UUID
import enum

from pydantic import BaseModel, Extra


class GroundstationRepresentation(BaseModel):
    gs_id: str
    gs_name: str
    gs_hostname: Optional[str]
    gs_port: Optional[int]
    commission_date: datetime
    latitude: Union[float, int]
    longitude: Union[float, int]
    altitude: Union[float, int]
    min_elevation: Optional[float]
    max_elevation: Optional[float]
    az_limit_start: Optional[float]
    az_limit_end: Optional[float]
    address: Optional[str]
    config: Optional[str]
    publicly_visible: bool
    telecoms: Optional[list[Any]]
    provider: Optional[str]
    provider_ref: Optional[str]

    class Config:
        allow_mutation = True


class GroundstationList(BaseModel):
    page: int
    total_pages: int
    gs_results: list[GroundstationRepresentation]


class SatTelecom(BaseModel):
    transmitting_encrypted: bool
    receiving_encrypted: bool
    tx_bitrate: Optional[int]
    rx_bitrate: Optional[int]
    tx_payload_size: Optional[int]
    rx_payload_size: Optional[int]
    tx_checksum_enabled: Optional[bool]
    rx_checksum_enabled: Optional[bool]
    tx_rs_enabled: Optional[bool]
    rx_rs_enabled: Optional[bool]
    tx_cc_enabled: Optional[bool]
    rx_cc_enabled: Optional[bool]


class Config(BaseModel):
    gs_provider: Optional[str]
    gs_hostname: Optional[str]
    gs_port: Optional[int]
    gs_telecom_port: Optional[int]
    link_band: Optional[str]
    transmitting_encrypted: bool
    receiving_encrypted: bool
    sat_telecom: Optional[SatTelecom]


class OverpassRepresentation(BaseModel):
    id: UUID
    sat_id: str
    gs_id: str
    rise_time: Optional[datetime]
    set_time: Optional[datetime]
    aos: datetime
    tca: datetime
    los: datetime
    maximum_el: float
    trajectory: dict[str, Any]
    tle_updated_at: datetime
    config: Optional[Config]
    source: str
    source_reference: Optional[str]


class OverpassList(BaseModel):
    __root__: list[OverpassRepresentation]


class Overpass(OverpassRepresentation):
    gs: GroundstationRepresentation
    maximum_el_time: Optional[datetime]


class TelemetryHistoryRepresentation(BaseModel):
    tlm_id: str  # unsure if optional
    tlm_name: str  # unsure if optional
    observed_at: datetime
    received_at: datetime
    value: Any
    raw: Any


class TelemetryHistory(BaseModel):
    tlm_id: Optional[str]
    tlm_name: Optional[str]
    cursor: Optional[str]
    total_pages: int
    telemetry: list[TelemetryHistoryRepresentation]


class FileStatisticsRepresentation(BaseModel):
    file_id: int
    component_id: Optional[str]
    max_complete_entry_id: Optional[int]
    last_received_at: Optional[datetime]


class FileStatisticsList(BaseModel):
    file_downlink_statistics: list[FileStatisticsRepresentation]


class TelemetryLatestRepresentation(BaseModel):
    observed_at: Optional[datetime]
    received_at: Optional[datetime]
    tlm_id: str
    tlm_name: str
    value: Any
    raw: Any


class LatestBy(str, enum.Enum):
    OBSERVED_AT = "observed_at"
    RECEIVED_AT = "received_at"


class TelemetryLatest(BaseModel):
    cursor: Optional[str]
    total_pages: int
    latestby: Optional[LatestBy]
    telemetry: List[TelemetryLatestRepresentation]


class TleRepresentation(BaseModel):
    norad_cat_id: int
    tle_line_0: str
    tle_line_1: str
    tle_line_2: str
    last_updated: datetime
    object_type: Optional[str]
    retrieval_time: datetime


class FileDownlinkRepresentation(BaseModel):
    id: str
    sat_id: str
    instruction_id: Optional[int]
    file_id: str
    component_id: Optional[str]
    status: str
    created_at: datetime
    started_at: Optional[datetime]
    concluded_at: Optional[datetime]
    timeout: Optional[datetime]

    first_entry_id: Optional[int]
    final_entry_id: Optional[int]
    total_entry_count: Optional[int]
    current_entry_count: Optional[int]

    failed_entries: dict[str, str]


class FileDownlinkListResponse(BaseModel):
    file_downlinks: list[FileDownlinkRepresentation]
    cursor: Optional[str]
    total_pages: int
