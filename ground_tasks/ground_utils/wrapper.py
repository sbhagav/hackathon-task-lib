from datetime import datetime, timedelta
from typing import Any, Literal, Optional, Union

from skyfield.api import load, EarthSatellite, wgs84
from skyfield.timelib import Timescale
from quindar_tdk.core.context import GroundTaskContext
import numpy as np

import requests

from config import ApiSettings
from schemas import (
    FileDownlinkListResponse,
    FileDownlinkRepresentation,
    FileStatisticsList,
    FileStatisticsRepresentation,
    GroundstationList,
    GroundstationRepresentation,
    Overpass,
    OverpassList,
    OverpassRepresentation,
    TelemetryHistory,
    TelemetryHistoryRepresentation,
    TelemetryLatest,
    TelemetryLatestRepresentation,
    TleRepresentation,
)


class Client:
    def __init__(
            self,
            task_context: GroundTaskContext,
            verbose: bool = True,
    ):
        api_settings = ApiSettings(task_context)

        self.base_url = api_settings.base_url
        self.api_key: str = api_settings.api_key
        self.refresh_token: Optional[str] = None
        self.sat_id = api_settings.sat_id
        self.verbose = verbose
        self.session = requests.Session()
        self.headers = {"x-saber-api-key": self.api_key, "accept": "application/json"}

    def _call_api(
            self, endpoint: str, payload: Optional[dict[str, Any]] = None
    ) -> dict[str, Any]:
        """Make a request to the API and return the response as a dictionary"""

        # Check if we have a token, if not, login
        if self.api_key is None:
            raise ValueError("Must provide api_key in secret.py")

        # Use the requests library to make the API call
        response = self.session.get(
            f"{self.base_url}/{endpoint}", params=payload, headers=self.headers
        )

        if response.status_code != 200 and self.verbose:
            print(
                f"API returned status code {response.status_code}.\n Request: {response.url}\n Message: {response.text}"
            )

        # Return the response as a dictionary
        try:
            return response.json()
        except:
            raise ValueError(f"Failed to parse response.\n Message: {response.text}")

    def _post_api(self, endpoint: str, payload: dict[str, Any]) -> dict[str, Any]:
        """Make a request to the API and return the response as a dictionary"""

        # Set the authorization header using the stored access_token
        headers: dict[str, str] = (
            {"Authorization": f"Bearer {self.api_key}"}
            if self.api_key
            else {}
        )
        # Use the requests library to make the API call
        response = self.session.post(
            f"{self.base_url}/{endpoint}", json=payload, headers=self.headers
        )
        # Return the response as a dictionary
        return response.json()

    def _get_overpasses(
            self,
            start_time: datetime = datetime.utcnow(),
            end_time: datetime = datetime.utcnow() + timedelta(hours=6),
    ) -> list[OverpassRepresentation]:
        """Get a list of overpasses"""
        params = {
            "sat_id": self.sat_id,
            "start_time": start_time.isoformat(),
            "end_time": end_time.isoformat(),
        }
        # Call the overpass_list endpoint
        response = self._call_api("overpass", params)

        overpasses = OverpassList.parse_obj(response).__root__

        return [i for i in overpasses if i.source != "api-autogen"]

    def _get_groundstations(self) -> list[GroundstationRepresentation]:
        """Get a list of groundstations"""
        # Call the groundstation_list endpoint
        response = self._call_api("groundstation", {})
        gs_list = GroundstationList(**response).gs_results
        return gs_list

    def _calculate_az_el_trajectory(
            self,
            start_time: datetime,
            end_time: datetime,
            gs: GroundstationRepresentation,
            tle: TleRepresentation,
            timescale: Timescale = load.timescale(),
            dt: float = 20.0,
    ):
        """Propagate a satellite with provided tle & calculate its azimuth/elevation trajectory relative to the given
        ground site, between the start and end times.

        Arguments:
            start_time (datetime.datetime): Beginning of trajectory calculation interval.
            end_time (datetime.datetime): End of trajectory calculation interval.
            gs (schemas.GroundstationRepresentation): Ground site & origin of az/el coordinates.
            tle (schemas.TleRepresentation): Two-line element set describing the satellite's orbit.
            timescale (skyfield.timelib.Timescale): Timescale object used for skyfield computations.
            dt (float, default=20.0): Sampling timestep, in seconds.

        Returns:
            times (list of datetimes, length n_times): Datetimes at which the az/el position was calculated.
            azimuths (list of floats, length n_times): Azimuth angle, in degrees, corresponding to the time at the same index.
            elevations (list of floats, length n_times): Elevation angle, in degrees, corresponding to the time at the same index.
        """
        sat = EarthSatellite(tle.tle_line_1, tle.tle_line_2, tle.tle_line_0, timescale)
        gs_pos = wgs84.latlon(gs.latitude, gs.longitude, gs.altitude)

        duration = (end_time - start_time).total_seconds()

        time_range = timescale.utc(
            year=start_time.year,
            month=start_time.month,
            day=start_time.day,
            hour=start_time.hour,
            minute=start_time.minute,
            second=np.arange(start_time.second, start_time.second + duration, dt),
        )

        times = []
        azimuths = []
        elevations = []

        for t in time_range:
            el, az, _ = (sat - gs_pos).at(t).altaz("standard")
            times.append(t.utc_datetime())
            azimuths.append(az.degrees)
            elevations.append(el.degrees)

        return times, azimuths, elevations

    def get_overpasses(
            self,
            start_time: datetime = datetime.utcnow(),
            end_time: datetime = datetime.utcnow() + timedelta(hours=6),
    ) -> list[Overpass]:
        """Get a list of overpasses"""
        overpasses_repr = self._get_overpasses(start_time, end_time)
        groundstations = self._get_groundstations()
        gs_mapping = {gs.gs_id: gs for gs in groundstations}

        overpasses = [
            Overpass.parse_obj(
                {
                    **overpass_repr.dict(),
                    "gs": gs_mapping[overpass_repr.gs_id],
                    "maximum_el_time": overpass_repr.tca,
                }
            )
            for overpass_repr in overpasses_repr
        ]

        return overpasses

    def get_telemetry_latest(
            self,
            component: Optional[str] = None,
            packet: Optional[str] = None,
            tlm_name: Optional[Union[str, list[str], set[str]]] = None,
            tlm_id: Optional[Union[str, list[str], set[str]]] = None,
            latest_by: Optional[Literal["observed_at", "received_at"]] = "observed_at",
            page_size: Optional[int] = 20000,
    ) -> list[TelemetryLatestRepresentation]:
        """Get latest telemetry"""
        params: dict[str, Any] = {}
        if component:
            params["component"] = component
        if packet:
            params["packet"] = packet
        if tlm_name:
            params["tlm_name"] = tlm_name
        if tlm_id:
            params["tlm_id"] = tlm_id
        if latest_by:
            params["latestby"] = latest_by
        if page_size:
            params["page_size"] = str(page_size)

        telemetry: list[TelemetryLatestRepresentation] = []

        # Call the telemetry_latest endpoint
        response = self._call_api(f"satellite/{self.sat_id}/latesttelemetry", params)
        telemetry_response = TelemetryLatest.parse_obj(response)
        telemetry += telemetry_response.telemetry

        # If there are more pages, call the endpoint again with the cursor
        if (
                str(len(telemetry)) == params["page_size"]
                and telemetry_response.cursor
                and telemetry_response.total_pages > 1
        ):
            previous_cursor = ""
            while telemetry_response.cursor:
                if telemetry_response.cursor == previous_cursor:
                    break
                params["cursor"] = telemetry_response.cursor
                response = self._call_api(
                    f"satellite/{self.sat_id}/latesttelemetry", params
                )
                previous_cursor = telemetry_response.cursor
                telemetry_response = TelemetryLatest.parse_obj(response)
                telemetry += telemetry_response.telemetry

        return telemetry

    def get_telemetry_history(
            self,
            component: Optional[str] = None,
            packet: Optional[str] = None,
            tlm_name: Optional[Union[str, list[str], set[str]]] = None,
            observed_at_start: Optional[datetime] = None,
            observed_at_end: Optional[datetime] = None,
            remove_duplicates: bool = True,
            page_size: int = 20000,
    ) -> list[TelemetryHistoryRepresentation]:
        """Get telemetry history"""
        params: dict[str, Union[str, list[str], set[str]]] = {}
        if component:
            params["component"] = component
        if packet:
            params["packet"] = packet
        if tlm_name:
            params["tlm_name"] = tlm_name
        if observed_at_start:
            params["observed_at_start"] = observed_at_start.isoformat()
        if observed_at_end:
            params["observed_at_end"] = observed_at_end.isoformat()
        if page_size:
            params["page_size"] = str(page_size)

        telemetry: list[TelemetryHistoryRepresentation] = []

        # Call the telemetry_history endpoint
        response = self._call_api(f"satellite/{self.sat_id}/telemetry", params)
        telemetry_response = TelemetryHistory.parse_obj(response)
        telemetry += telemetry_response.telemetry

        if str(len(telemetry)) == params["page_size"] and telemetry_response.cursor:
            previous_cursor = ""
            while telemetry_response.cursor:
                if telemetry_response.cursor == previous_cursor:
                    break
                params["cursor"] = telemetry_response.cursor
                response = self._call_api(f"satellite/{self.sat_id}/telemetry", params)
                previous_cursor = telemetry_response.cursor
                telemetry_response = TelemetryHistory.parse_obj(response)
                telemetry += telemetry_response.telemetry

        # remove telemetry with the same observed_at and raw
        if remove_duplicates:
            telemetry = list(
                {
                    tlm.observed_at.isoformat() + str(tlm.raw): tlm for tlm in telemetry
                }.values()
            )
        return telemetry

    def get_file_statistics(
            self,
            file_id: Optional[str] = None,
            component: Optional[str] = None,
    ) -> list[FileStatisticsRepresentation]:
        """Get file statistics"""
        params = {}
        if file_id:
            params["file_id"] = file_id
        if component:
            params["component_id"] = component

        # Call the file_statistics endpoint
        response = self._call_api(f"satellite/{self.sat_id}/filestatistics", params)
        return FileStatisticsList.parse_obj(response).file_downlink_statistics

    def get_tle(self, norad_id: int) -> TleRepresentation:
        """Get TLE"""
        response = self._call_api(f"tle/{norad_id}")
        return TleRepresentation.parse_obj(response)

    def get_file_downlinks(
            self,
            status: list[str] = [],
            file_id: Optional[str] = None,
            component_id: Optional[str] = None,
            max_downlinks: int = 500,
            page_size: int = 500,
    ) -> list[FileDownlinkRepresentation]:
        """
        Get file downlinks

        Args:
            status: list of statuses to filter by
                e.g. ['pending', 'downloading', 'complete', 'degraded', 'failed', 'terminated', 'expired']
            file_id: file id to filter by
            component_id: component id to filter by
            max_downlinks: maximum number of downlinks to return
            page_size: number of downlinks to return per page (max 500)

        Returns:
            list of file downlinks
        """

        params: dict[str, Any] = {}
        if status:
            params["status"] = status
        if file_id:
            params["file_id"] = file_id
        if component_id:
            params["component_id"] = component_id
        if page_size:
            if page_size > 500:
                page_size = 500
                print("Warning: page_size cannot be greater than 500. Setting to 500.")
            params["page_size"] = str(page_size)

        file_downlinks: list[FileDownlinkRepresentation] = []

        # Call the file_statistics endpoint
        response = self._call_api(f"satellite/{self.sat_id}/filedownlinks", params)
        file_downlinks_response = FileDownlinkListResponse.parse_obj(response)
        file_downlinks += file_downlinks_response.file_downlinks

        # If there are more pages, call the endpoint again with the cursor
        if file_downlinks_response.cursor and file_downlinks_response.total_pages > 1:
            previous_cursor = ""
            while (
                    file_downlinks_response.cursor and len(file_downlinks) < max_downlinks
            ):
                if file_downlinks_response.cursor == previous_cursor:
                    break
                params["cursor"] = file_downlinks_response.cursor
                response = self._call_api(
                    f"satellite/{self.sat_id}/filedownlinks", params
                )
                previous_cursor = file_downlinks_response.cursor
                file_downlinks_response = FileDownlinkListResponse.parse_obj(response)
                file_downlinks += file_downlinks_response.file_downlinks

        # Limit the number of downlinks returned to max_downlinks
        file_downlinks = file_downlinks[:max_downlinks]

        return file_downlinks
