import os
import json
from quindar_tdk.core.context import GroundTaskContext
import boto3


# Example login details
def get_credentials(task_context: GroundTaskContext):
    if task_context.cloud:
        secrets_manager = boto3.client('secretsmanager')
        secret = f"{task_context.tenant_id}/{task_context.environment}/scepter/keys"
        auth_key = json.loads(secrets_manager.get_secret_value(SecretId=secret)["SecretString"])
        key = auth_key["saber_api_key"]
        url = auth_key["saber_base_url"]
    else:
        key = os.getenv("SABER_API_KEY")
        if not key:
            raise AttributeError("Must have SABER_API_KEY in environment variables if running locally")

        url = os.getenv("SABER_BASE_URL")
        if not url:
            raise AttributeError("Must have SABER_BASE_URL in environment variables if running locally")

    return key, url
