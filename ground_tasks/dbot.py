from datetime import datetime, timedelta
from enum import Enum
from ground_utils.wrapper import Client
from quindar.api import QClient, QApiError, QLibError
from quindar.api.resource import ContactExecutionQueues, ContactTasks, Spacecrafts, ContactTaskDefinitions, \
    SpacecraftParameters, Contacts
from quindar.models.resources import ContactTask, Spacecraft, SpacecraftParameter, ContactTaskStatus
from quindar_tdk.core.context import GroundTaskContext
import logging
import json
import os
import csv
from dateutil import parser

OUTPUT_CSV_FILE_NAME = 'dbotentryinfo.csv'

ATTRIBUTE_LIST = ['time', 'file_id', 'last_entry_id', 'board', 'stack', 'max_complete_entry_id', 'packet_name',
                  'storage_rate', 'max_entries_stored', 'max_time_stored_s', 'max_commands_required', 'num_id_stale',
                  'time_stale_hrs', 'command_entries']

CSV_MAPPING = {
    'packet_name': 'packet name',
    'file_id': 'file id',
    'board': 'board',
    'stack': 'stack',
    'time': 'time',
    'last_entry_id': 'last entry id',
    'max_complete_entry_id': 'max complete entry id',
    'max_entries_stored': 'max entries stored'
}
CSV_ROWS = list(CSV_MAPPING.values())
MAX_ENTRIES_PER_COMMAND = int(os.getenv("MAX_ENTRIES_PER_COMMAND", 1520))


class Stack(Enum):
    """Enum for the different stacks."""

    YM = "YM"
    YP = "YP"


class Board(Enum):
    APC = "APC"
    FC = "FC"


def execute(quindar_client: QClient, parameters: dict, context: GroundTaskContext):
    context.log.info(f"Running with parameters: {parameters}")
    spacecraft_client = Spacecrafts(api_client=quindar_client)
    contact_client = Contacts(api_client=quindar_client)

    if 'spacecraft_name' in parameters:
        spacecraft_name = parameters['spacecraft_name']
    else:
        spacecraft_name = 'Bluewalker-3'

    filters = {'name': {'_eq': spacecraft_name}}
    resp = spacecraft_client.get(filters)
    spacecraft = resp.response[0]

    context.log.info(f"Running dbot ground task for {spacecraft.name}. Spacecraft ID = {spacecraft.id}")

    if 'add_to_queue' in parameters:
        if parameters['add_to_queue'].lower() in ['false', 'no']:
            add_to_queue = False
        else:
            add_to_queue = True
    else:
        add_to_queue = False

    if not add_to_queue:
        context.log.info("add_to_queue is set to false. Not adding tasks to contact queue")

    if 'next_contact_aos' in parameters:
        next_contact_aos = parser.parse(parameters['next_contact_aos'])
    else:
        filters = {
            'spacecraft_id': {'_eq': str(spacecraft.id)},
            'contact_start_time': {'_gte': datetime.utcnow().isoformat(),
                                   '_lte': (datetime.utcnow() + timedelta(hours=24)).isoformat()}
        }
        resp = contact_client.get(filters=filters, order_by=[{"contact_start_time": "asc"}], limit=1)
        if len(resp.response) == 1:
            next_contact_aos = resp.response[0].contact_start_time
        else:
            next_contact_aos = datetime.utcnow() + timedelta(minutes=90)
            context.log.info("Could not find any contacts within 24 hours. Assuming next contact is in 90 minutes")

    context.log.info(f"Assuming next contact at {next_contact_aos}")

    if 'stack' in parameters:
        stack = Stack(parameters['stack'].upper())
    else:
        stack = get_current_stack(spacecraft, quindar_client, context=context)
    context.log.info(f"Running on stack {stack}")

    if 'packet_list' in parameters:
        packet_list = parameters['packet_list'].split(',')
    else:
        packet_list = ["POWER_PCDU_LVC_TLM", "POWER_CSBATS_TLM", "AOCS_TLM", "FSW_TLM_APC", "FSW_TLM_FC",
                       "MEDIC_LEADER_TLM", "MEDIC_FOLLOWER_TLM_FC", "MODE_SWITCHING_TLM", "FDIR_TLM_APC", "FDIR_TLM_FC",
                       "SCRIPT_ENGINE_FDIR_TLM", "FUNCTION_RUNNER_FLAGS_TLM_APC", "THERMAL_TLM", "COMM_TLM",
                       "FUNCTION_RUNNER_TLM_APC"]

    setup_output_csv()

    context.log.info(f"Configuring Download Backorbit Telemetry (DBOT) for packets {packet_list}")
    saber_client = Client(task_context=context)

    for packet in packet_list:
        packet = PacketInfo(packet_name=packet, spacecraft=spacecraft, quindar_client=quindar_client,
                            saber_client=saber_client, next_contact_aos=next_contact_aos, context=context)
        if packet.file_id in [4125, 4126]:
            packet.stack = stack.value
            packet.max_complete_entry_id = 0
            packet.last_entry_id = 0
        else:
            if packet.board == "APC":
                packet.get_packet_data(active_stack=stack.value, client=saber_client)
            else:
                packet.get_packet_data(active_stack=stack.value, client=saber_client)

        packet.update_spacecraft_parameter()
        packet.update_csv()

    # context.write_file_to_s3(file_path=OUTPUT_CSV_FILE_NAME)


def setup_output_csv():
    with open(OUTPUT_CSV_FILE_NAME, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(CSV_ROWS)


def get_current_stack(spacecraft: Spacecraft, quindar_client: QClient, context: GroundTaskContext) -> Stack:
    spacecraft_params_client = SpacecraftParameters(api_client=quindar_client)
    try:
        stack_param = spacecraft_params_client.get_latest_value(spacecraft.id, "STACK")
        return Stack[stack_param.value.upper()]
    except QApiError:
        context.log.warning("No spacecraft parameter for 'STACK'. Using 'YP'")
        return Stack.YP


class PacketInfo(object):
    file_id: [int, None] = None
    storage_rate: [float, None] = None
    last_entry_id: [int, None] = None
    max_complete_entry_id: [int, None] = None
    time: [str, datetime] = None
    stack: [str, Stack] = None
    board: [str, Board] = None
    max_entries_stored: [int, None] = None
    max_time_stored_s: [int, None] = None
    max_commands_required: [float, None] = None
    num_id_stale: [int, None] = None
    time_stale_hrs: [float, None] = None
    command_entries: [dict] = list()

    def __init__(self, packet_name: str, spacecraft: Spacecraft, quindar_client: QClient, saber_client: Client,
                 next_contact_aos: datetime, context: GroundTaskContext):
        self.packet_name = packet_name
        self.spacecraft = spacecraft
        self.quindar_client = quindar_client
        self.saber_client = saber_client
        self.spacecraft_parameter_name = f"{self.packet_name.upper()}_INFO"
        self._get_packet_parameters()
        self.next_contact_aos = next_contact_aos
        self.context = context

    def _get_packet_parameters(self):
        """
        Queries spacecraft parameters to get packet information
        """
        spacecraft_params_client = SpacecraftParameters(api_client=self.quindar_client)
        try:
            packet_info = spacecraft_params_client.get_latest_value(self.spacecraft.id, self.spacecraft_parameter_name)
            packet_info = json.loads(packet_info.value)
            if len(packet_info['board'].split('_')) == 2:
                self.board = packet_info['board'].split('_')[0]
                self.stack = packet_info['board'].split('_')[1]
            else:
                self.board = packet_info['board']
                self.stack = packet_info['stack']

            for attribute in ATTRIBUTE_LIST:
                try:
                    if getattr(self, attribute) is None:
                        setattr(self, attribute, packet_info[attribute])
                except KeyError:
                    self.context.log.warning(f"No spacecraft parameter for {attribute}")
                if getattr(self, attribute) in [None, '']:
                    setattr(self, attribute, self._get_default_value(attribute))
        except (QLibError, QApiError) as e:
            self.context.log.warning(f"No spacecraft parameter for {self.spacecraft_parameter_name}. Initializing from defaults")
            for attribute in ATTRIBUTE_LIST:
                setattr(self, attribute, self._get_default_value(attribute))

        self.command_entries = list()
        self.max_entries_stored = float(self.max_entries_stored)
        self.storage_rate = float(self.storage_rate)
        self.max_entries_stored = int(self.max_entries_stored)
        self.max_time_stored_s = self.max_entries_stored / self.storage_rate
        self.max_commands_required = self.max_entries_stored / int(os.getenv('MAX_ENTRIES_PER_COMMAND', 1520))

    def get_packet_data(self, active_stack: str, client: Client) -> bool:
        """
        Queries telemetry to get latest information for packet information
        """

        data = False
        for i in range(4):
            observed_at_start = datetime.now() - timedelta(days=2 ** i)
            try:
                self.context.log.info(f"Board: {self.board}")
                last_entry_id, max_complete_entry_id = get_data(client=client, stack=active_stack,
                                                                observed_at_start=observed_at_start,
                                                                board=self.board, file_id=self.file_id)
                data = True
                break
            except IndexError:
                continue

        if not data:
            observed_at_start = datetime.min
            try:
                self.context.log.info(f"Board: {self.board}")
                self.context.log.info(f"File id: {self.file_id}")
                last_entry_id, max_complete_entry_id = get_data(client=client, stack=active_stack,
                                                                observed_at_start=observed_at_start,
                                                                board=self.board, file_id=self.file_id)
            except IndexError:
                raise IndexError(f"Not able to get the data for file id {self.file_id}")

        self.last_entry_id = last_entry_id.value
        self.max_complete_entry_id = max_complete_entry_id
        self.time = last_entry_id.observed_at
        current_command_end_entry = (self.next_contact_aos - self.time).seconds * self.storage_rate + self.last_entry_id
        current_command_start_entry = current_command_end_entry - (MAX_ENTRIES_PER_COMMAND - 1)
        max_latest_end_entry = current_command_end_entry
        while current_command_start_entry > max(self.max_complete_entry_id,
                                                max_latest_end_entry - self.max_entries_stored):
            self.command_entries.append({'start': current_command_start_entry, 'end': current_command_end_entry})
            current_command_end_entry = current_command_start_entry - 1
            current_command_start_entry = current_command_end_entry - (MAX_ENTRIES_PER_COMMAND - 1)
        self.command_entries.append({'start': self.max_complete_entry_id + 1, 'end': current_command_end_entry})
        if len(self.command_entries) - 1 > self.max_commands_required:
            self.context.log.error(f"Gather too many commands for {self.packet_name}. Exiting")
            raise ValueError(f"Found too many commands for {self.packet_name}")

        if len(self.command_entries) > 2:
            self.context.log.info("More than 2 commands necessary to download all data. Adding to queue")
            self.add_dbot_to_queue()
        self.num_id_stale = self.command_entries[0]['end'] - self.max_complete_entry_id
        self.num_id_stale = self.last_entry_id - self.max_complete_entry_id
        self.time = self.time.strftime("%Y-%m-%d %H:%M:%S")
        self.stack = active_stack
        self.time_stale_hrs = self.num_id_stale / self.storage_rate / (60 * 60)

        return True

    def add_dbot_to_queue(self):
        contact_queue_client = ContactExecutionQueues(api_client=self.quindar_client)
        contact_task_client = ContactTasks(api_client=self.quindar_client)
        task_definition_client = ContactTaskDefinitions(api_client=self.quindar_client)
        task_definition_name = os.getenv('DBOT_TASK_DEFINITION_NAME', 'FSW_1003_File_Download_Backorbit_Sparse_Auto')
        resp = task_definition_client.get({'name': {'_eq': task_definition_name}})
        if len(resp.response) == 0:
            raise AttributeError(f"Could not find task definition {task_definition_name}")
        task_definition = resp.response[0]

        contact_queue = contact_queue_client.get_queue(spacecraft_id=self.spacecraft.id)
        param_packet_value = [f'{self.packet_name} {self.file_id}']
        add_task = True
        for contact_task in contact_queue.contact_tasks:
            filters = {
                'id': {'_eq': str(contact_task["id"])}
            }
            resp = contact_task_client.get(filters)
            task = resp.response[0]
            if str(task.task_def) == str(task_definition.id) and param_packet_value == task.task_params["tlm_packets"]:
                add_task = False
                # resp = contact_queue_client.remove_task_from_queue(spacecraft_id=self.spacecraft.id,
                #                                                    task_id=task.id)
                # if resp.status_code != 200:
                #     logger.warning("Could not clean queue")
                #     return
        if add_task:
            task_params = {
                "boards": [f'{self.board}_{self.stack}'],
                "link_type": "SBAND_HIGH",
                "tlm_packets": param_packet_value,
                "use_fsw_info": True,
                "PKT_SIZE_BUFFER": 1,
                "retry_threshold": 75,
                "forced_sparseness": 24,
                "contact_duration": 600
            }
            resp = contact_task_client.post(
                ContactTask(
                    spacecraft_id=self.spacecraft.id,
                    task_def=task_definition.id,
                    task_params=task_params,
                    status=ContactTaskStatus.PLANNED
                )
            )
            if resp.status_code != 201:
                self.context.log.error(f"Could not add task to queue for {self.packet_name}")

    def _get_default_value(self, attribute):
        current_directory = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(current_directory, 'default_packet_info.csv'), 'r', encoding='utf-8-sig') as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                if row['packet_name'] == self.packet_name:
                    try:
                        if row[attribute] == '':
                            return None
                        return row[attribute]
                    except KeyError:
                        return None

    def update_spacecraft_parameter(self):
        spacecraft_parameters_client = SpacecraftParameters(self.quindar_client)
        parameter_value = dict()
        for attribute in ATTRIBUTE_LIST:
            parameter_value[attribute] = getattr(self, attribute)

        spacecraft_parameters_client.post(
            SpacecraftParameter(
                name=self.spacecraft_parameter_name,
                value=json.dumps(parameter_value),
                spacecraft_id=self.spacecraft.id
            )
        )

    def update_csv(self):
        with open(OUTPUT_CSV_FILE_NAME, 'a', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            row_data = list()
            for attribute in list(CSV_MAPPING.keys()):
                row_data.append(getattr(self, attribute))
            csvwriter.writerow(row_data)


def get_data(client: Client, stack, observed_at_start: datetime, board, file_id):
    file_ids = client.get_telemetry_history(
        packet="FILE_INFO_RES",
        component=f"{board}_{stack}",
        tlm_name="FILE_ID",
        observed_at_start=observed_at_start,
        observed_at_end=datetime.utcnow(),
    )

    last_entries = client.get_telemetry_history(
        packet="FILE_INFO_RES",
        component=f"{board}_{stack}",
        tlm_name="LAST_ENTRY_ID",
        observed_at_start=observed_at_start,
        observed_at_end=datetime.utcnow(),
    )
    max_complete = client.get_file_statistics(component=f"{board}_{stack}")

    try:
        newest = next(x for x in file_ids if int(x.value) == int(file_id))
    except StopIteration:
        raise IndexError(
            f"""The required data wasn't found, you should do a FILE_INFO request for all packets. As a
                temporary workaround, you can set the 'extendedRange' parameter to True, but this
                will only work for a short time. To properly resolve this error, keep the FILE_INFOs up to date."""
        )
    observed = newest.observed_at
    last_entry_id = next(x for x in last_entries if x.observed_at == observed)
    try:
        max_complete_entry_id = next(
            x.max_complete_entry_id for x in max_complete if x.file_id == file_id
        )
    except:
        max_complete_entry_id = 0

    return last_entry_id, max_complete_entry_id,

# def get_file_info_data(saber_client: Client, stack: str, file_id: int, board: str, file_ids=None):
#     data = False
#     observed_at_start = datetime.now()
#     for i in range(4):
#         if file_ids:
#             try:
#                 newest = next(x for x in file_ids if int(x.value) == int(file_id))
#                 data = True
#                 break
#             except StopIteration:
#                 pass
#         observed_at_start = datetime.now() - timedelta(days=2 ** i)
#         file_ids = saber_client.get_telemetry_history(
#             packet="FILE_INFO_RES",
#             component=f"{board}_{stack}",
#             tlm_name="FILE_ID",
#             observed_at_start=observed_at_start,
#             observed_at_end=datetime.utcnow(),
#         )
#
#     if not data:
#         logger.warning(f"No data since {observed_at_start}. Checking for case of stack change.")
#         observed_at_start = datetime.min
#         file_ids = saber_client.get_telemetry_history(
#             packet="FILE_INFO_RES",
#             component=f"{board}_{stack}",
#             tlm_name="FILE_ID",
#             observed_at_start=observed_at_start,
#             observed_at_end=datetime.utcnow(),
#         )
#         try:
#             newest = next(x for x in file_ids if int(x.value) == int(file_id))
#         except StopIteration:
#             raise AttributeError(f"No FILE_INFO_RES for FILE_ID {file_id}")
#     timestamp = newest.observed_at
#
#     if timestamp == datetime.min:
#         observed_at_start = datetime.min
#         observed_at_end = datetime.min + timedelta(minutes=1)
#     else:
#         observed_at_start = timestamp - timedelta(minutes=30)
#         observed_at_end = timestamp + timedelta(minutes=30)
#     # get last_entry_id near timestamp
#
#     last_entry = saber_client.get_telemetry_history(
#         packet="FILE_INFO_RES",
#         component=f"{board}_{stack}",
#         tlm_name="LAST_ENTRY_ID",
#         observed_at_start=observed_at_start,
#         observed_at_end=observed_at_end
#     )
#     try:
#         last_entry_id = next(x for x in last_entry if x.observed_at == timestamp)
#     except StopIteration:
#         raise AttributeError(f"Could not find last entry for {file_id}")
#
#     return timestamp, last_entry_id

# logger.info("Obtaining data")
# items = get_last_entry(client=saber_client, stack=stack)
# print("Building CSV...")
# create_csv(items, spacecraft, quindar_client)
# # update_parameters()
#
# write_file_to_s3(DBOT_FILE_NAME, task_context=context)

# spacecraft_client = Spacecrafts(api_client=quindar_client)
# contact_task_client = ContactTasks(api_client=quindar_client)
# contact_task_definitions = ContactTaskDefinitions(api_client=quindar_client)
# filters = {'name': {'_eq': spacecraft_name}}
# spacecraft = spacecraft_client.get(filters=filters).response[0]
# filters = {'name': {'_eq': 'FSW_1003_File_Download_Backorbit_Sparse_Auto'}}
# contact_task_definition = contact_task_definitions.get(filters=filters).response[0]
# task_params = {
#     "boards": [
#         "APC_YP"
#     ],
#     "link_type": "SBAND_HIGH",
#     "tlm_packets": [
#         "POWER_CSBATS_TLM 1039",
#         "POWER_PCDU_LVC_TLM 1026"
#     ],
#     "use_fsw_info": True,
#     "use_alt_stack": False,
#     "PKT_SIZE_BUFFER": 1,
#     "retry_threshold": 80,
#     "forced_sparseness": 24,
#     "contact_duration_s": 240,
#     "max_download_duration_s": 120,
#     "max_entries_to_download": 39050
# }
# resp = contact_task_client.insert_tasks(tasks=[
#     ContactTask(
#         spacecraft_id=spacecraft.id,
#         task_def=contact_task_definition.id,
#         task_params=task_params,
#         status='PLANNED',
#         updated_at=datetime.utcnow()
#     )], alert=False
# )
# contact_task = resp.response[0]
# queue_client = ContactExecutionQueues(api_client=quindar_client)
# queue_client.get_queue(spacecraft_id=spacecraft.id)
# queue_client.append_task_to_queue(spacecraft_id=spacecraft.id, task_id=contact_task.id)
