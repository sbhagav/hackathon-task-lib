import csv
import os
from datetime import datetime, timedelta
from enum import Enum
from typing import Any
from ground_utils.wrapper import Client
from quindar.api import QClient
from quindar.api.resource import ContactExecutionQueues, ContactTasks, Spacecrafts, ContactTaskDefinitions
from quindar.models.resources import ContactTask
from quindar_tdk.core.context import GroundTaskContext
import logging

DBOT_FILE_NAME = 'dbotentryinfo.csv'


class Stack(Enum):
    """Enum for the different stacks."""

    YM = "YM"
    YP = "YP"


class Task:
    spacecraft_name: str = "BLUEWALKER 1"
    current_stack: str = Stack.YP

    def run(self, quindar_client: QClient, parameters: dict, context: GroundTaskContext):
        """Main function."""
        client = Client(task_context=context)
        print("Obtaining data...")
        items = get_last_entry(client=client, stack=self.current_stack)

        print("Building CSV...")
        local_path = os.path.join(context.local_output_dir, DBOT_FILE_NAME)
        create_csv(local_path, items)
        context.write_file_to_s3(local_path, object_key=DBOT_FILE_NAME)


        spacecraft_client = Spacecrafts(api_client=quindar_client)
        contact_task_client = ContactTasks(api_client=quindar_client)
        contact_task_definitions = ContactTaskDefinitions(api_client=quindar_client)
        filters = {'name': {'_eq': self.spacecraft_name}}
        spacecraft = spacecraft_client.get(filters=filters).response[0]
        filters = {'name': {'_eq': 'FSW_1003_File_Download_Backorbit_Sparse_Auto'}}
        contact_task_definition = contact_task_definitions.get(filters=filters).response[0]
        task_params = {
            "boards": [
                "APC_YP"
            ],
            "link_type": "SBAND_HIGH",
            "tlm_packets": [
                "POWER_CSBATS_TLM 1039",
                "POWER_PCDU_LVC_TLM 1026"
            ],
            "use_fsw_info": True,
            "use_alt_stack": False,
            "PKT_SIZE_BUFFER": 1,
            "retry_threshold": 80,
            "forced_sparseness": 24,
            "contact_duration_s": 240,
            "max_download_duration_s": 120,
            "max_entries_to_download": 39050
        }
        resp = contact_task_client.insert_tasks(tasks=[
            ContactTask(
                spacecraft_id=spacecraft.id,
                task_def=contact_task_definition.id,
                task_params=task_params,
                status='PLANNED',
                updated_at=datetime.utcnow()
            )], alert=False
        )
        contact_task = resp.response[0]
        queue_client = ContactExecutionQueues(api_client=quindar_client)
        queue_client.get_queue(spacecraft_id=spacecraft.id)
        queue_client.append_task_to_queue(spacecraft_id=spacecraft.id, task_id=contact_task.id)


def get_data(
        client: Client, stack: Stack, observed_at_start: datetime
) -> list[dict[str, Any]]:
    id_list: dict[str, list[int]] = {
        "APC": [1026, 1039, 4122, 1030, 1038, 4123, 1032, 1028, 1027, 1031],
        "FC": [2049, 4122, 4123],
    }

    items: list[dict[str, Any]] = []

    for board in ["APC", "FC"]:

        file_ids = client.get_telemetry_history(
            packet="FILE_INFO_RES",
            component=f"{board}_{stack.value}",
            tlm_name="FILE_ID",
            observed_at_start=observed_at_start,
            observed_at_end=datetime.utcnow(),
        )

        last_entries = client.get_telemetry_history(
            packet="FILE_INFO_RES",
            component=f"{board}_{stack.value}",
            tlm_name="LAST_ENTRY_ID",
            observed_at_start=observed_at_start,
            observed_at_end=datetime.utcnow(),
        )

        max_complete = client.get_file_statistics(component=f"{board}_{stack.value}")

        for id in id_list[board]:
            item: dict[str, Any] = {}
            try:
                newest = next(x for x in file_ids if x.value == id)
            except StopIteration:
                raise IndexError(
                    f"""The required data wasn't found, you should do a FILE_INFO request for all packets. As a
                    temporary workaround, you can set the 'extendedRange' parameter to True, but this
                    will only work for a short time. To properly resolve this error, keep the FILE_INFOs up to date."""
                )
            observed = newest.observed_at
            last_entry_id = next(x for x in last_entries if x.observed_at == observed)
            try:
                max_complete_entry_id = next(
                    x.max_complete_entry_id for x in max_complete if x.file_id == id
                )
            except:
                max_complete_entry_id = 0
            item["Time"] = (observed + timedelta(seconds=0.5)).strftime(
                "%Y-%m-%d %H:%M:%S"
            )
            item["FILE_ID"] = id
            item["LAST_ENTRY_ID"] = last_entry_id.value
            item["Board"] = board + "_" + stack.value
            item["MAX_COMPLETE_ENTRY_ID"] = max_complete_entry_id

            items.append(item)
    return items


def get_last_entry(client: Client, stack: Stack = Stack.YP) -> list[dict[str, Any]]:
    # Try to get the data with an exponentially increasing range
    for i in range(4):
        observed_at_start = datetime.now() - timedelta(days=2 ** i)
        try:
            items = get_data(client, stack, observed_at_start=observed_at_start)
        except IndexError:
            continue
        else:
            break
    else:
        # Final try to get the data as far back as possible. This is to catch if there has been a stack change.
        try:
            items = get_data(client, stack, observed_at_start=datetime.min)
        except IndexError:
            raise IndexError("Not able to get the data")

    items.insert(6, items.pop())
    items.insert(3, items.pop())
    items.insert(2, items.pop())
    MEDIC_FOLLOWER = {
        "Time": "",
        "FILE_ID": 4125,
        "LAST_ENTRY_ID": "",
        "Board": f"FC_{stack.value}",
        "MAX_COMPLETE_ENTRY_ID": 0,
    }
    MEDIC_FOLLOWER["Time"] = items[5]["Time"]
    items.insert(6, MEDIC_FOLLOWER)
    SCRIPT_ENGINE = {
        "Time": "",
        "FILE_ID": 4126,
        "LAST_ENTRY_ID": "",
        "Board": f"APC_{stack.value}",
        "MAX_COMPLETE_ENTRY_ID": 0,
    }
    SCRIPT_ENGINE["Time"] = items[9]["Time"]
    items.insert(10, SCRIPT_ENGINE)

    return items


def create_csv(filename, items: list[dict[str, str]]) -> None:
    data_headers = [
        "FILE_ID",
        "Board",
        "LAST_ENTRY_ID",
        "Time",
        "",
        "MAX_COMPLETE_ENTRY_ID",
    ]

    with open(filename, "w") as f:
        writer = csv.DictWriter(f, fieldnames=data_headers)
        writer.writeheader()
        writer.writerows(items)


def execute(quindar_client: QClient, parameters: dict, context: GroundTaskContext):
    task = Task()
    task.run(quindar_client, parameters, context)