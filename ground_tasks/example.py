import csv
import pendulum, time
from quindar import QClient, PlatformConfigs, SystemEvents
from quindar.models import SystemEvent, EventType, EventLevel
from quindar_tdk.core.context import GroundTaskContext
from task_utils.example_util import gen_random

def execute(quindar_client: QClient, parameters: dict, context: GroundTaskContext):
    context.log.debug(f"this is a debug... you probly won't see it.")
    context.log.info(f"This is how you log in your task: {parameters}")
    context.log.info(f"This log message was committed from an AWS WorkSpace")
    
    time.sleep(3)
    context.log.info("let's write a csv!")
    field_names = ["id", "name", "value"]
    with open("/tmp/parameters.csv", "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=field_names)
        writer.writeheader()
        for x in range(1,15,1):
            writer.writerow({"id": "AST-TEST", "name": x, "value": gen_random()})
            
    context.write_file_to_s3("/tmp/parameters.csv", object_key="parameters.csv")

    time.sleep(3)
    context.log.info(f"let's publish an event!")
    system_event_client = SystemEvents(api_client=quindar_client)
    system_event_client.publish_event(SystemEvent(timestamp=pendulum.now(tz=pendulum.UTC),
                                                  source="ground-task-executor",
                                                  message=f"Test event from ground task",
                                                  event_type=EventType.GROUND_TASK_ERROR,
                                                  level=EventLevel.INFO,
                                                  anomaly_timestamp=pendulum.now(tz=pendulum.UTC)))
    # context.log.error("We have an error!")