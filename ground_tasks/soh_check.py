from quindar.api import QClient, Contacts
from pydantic import BaseModel
from uuid import UUID
from typing import Union, Optional
import enum
import logging
import datetime
import json
import os
import sys
from datetime import datetime, timedelta
from enum import Enum
from typing import Optional, Union
import toml  # type: ignore
from colorama import Fore
from colorama import init as colorama_init
from rich.console import Console, Group
from rich.table import Column, Table
from rich.text import Text
from ground_utils.schemas import (
    TelemetryHistoryRepresentation,
    TelemetryLatestRepresentation,
)
from ground_utils.wrapper import Client
from quindar.api.resource import ContactExecutionQueues, Spacecrafts, SpacecraftParameters, QApiError, Tickets
from quindar.models.resources import SpacecraftParameter
from quindar.models.amc import TicketCreate
from quindar_tdk.core.context import GroundTaskContext

output_message = ''

# constants
ISO_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
COLORAMA_IS_INITIALISED = False
SOH_FILE_NAME = 'SOH_TABLE.txt'
FDIR_FILE_NAME = 'FDIR_TABLE.txt'
FLAG_TRANSITION_FILE_NAME = 'FLAG_TRANSITION.txt'
OUTPUT_MESSAGE_FILE_NAME = 'OUTPUT.txt'

SPACECRAFT_NAME = "BLUEWALKER 1"


def execute(quindar_client: QClient, parameters: dict, context: GroundTaskContext):
    context.log.info(f"Starting SoH Check!")
    start = datetime.utcnow()
    client = Client(verbose=False, task_context=context)

    console = Console()

    display_options = {
        "show_health_check_id": False,
        "show_health_check_parameters": True,
        "show_FDIR_transitions": True,
        "show_FDIR_status_sums": False,
        "FDIR_hide_when_empty": ["NO_STATUS"],
    }

    main(client, console, display_options, quindar_client)
    delta = datetime.utcnow() - start
    print(f"\nTime taken: {round(delta.total_seconds(), 2):.2f} seconds")
    add_to_output_text(f"\nTime taken: {round(delta.total_seconds(), 2):.2f} seconds")

    context.write_file_to_s3(SOH_FILE_NAME, object_key=SOH_FILE_NAME)
    context.write_file_to_s3(FDIR_FILE_NAME, object_key=FDIR_FILE_NAME)
    context.write_file_to_s3(FLAG_TRANSITION_FILE_NAME, object_key=FLAG_TRANSITION_FILE_NAME)
    context.write_file_to_s3(OUTPUT_MESSAGE_FILE_NAME, object_key=OUTPUT_MESSAGE_FILE_NAME)


class Stack(Enum):
    """Possible vehicle stacks."""

    YM = "YM"
    YP = "YP"

    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class FDIRStatus(Enum):
    """Possible fault detection, isolation, and recovery (FDIR) system telemetry statuses."""

    NOT_MANAGED_OR_INACTIVE = "NOT_MANAGED_OR_INACTIVE"
    OK = "OK"
    YELLOW = "YELLOW"
    RED = "RED"
    NO_STATUS = "NO_STATUS"

    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class CheckStatus(Enum):
    """Possible state of health and FDIR check result statuses.

    PASS: Telemetry satisfied test conditions.
    FAIL: Telemetry did not satisfy test conditions.
    WARNING: Telemetry statisfied the test conditions, but was close to failing.
        TODO: Decide if this status is needed. It is not currently used.
    ERROR: Something went wrong while conducting the test.
        NOTE: Most likely scenario is that the telemetry has never been received before.
    INCOMPLETE: Sufficient data was not available, or it was too out-of-date to conduct the test.
    """

    PASS = "PASSED"
    FAIL = "FAILED"
    WARNING = "WARNING"
    ERROR = "ERROR"
    INCOMPLETE = "INCOMPLETE"


def initialise_colorama() -> None:
    """Initialise the Colorama module, used to colour text in the displayed output.
    NOTE: Initialising this module more than once for a session will cause issues.

    Arguments: None.

    Returns: None.

    References:
        Colorama GitHub repo: https://github.com/tartley/colorama
    """
    if not COLORAMA_IS_INITIALISED:
        colorama_init()


def get_component_packet_and_point_name_from_id(
        tlm_id: str,
) -> tuple[Union[str, None], Union[str, None], str]:
    """Get the telemetry point name and component from the unique telemetry point ID.
    NOTE: Cannot handle telemetry IDs which are unique to packet, but not unique to component, i.e. in the format
        "<packet_name>-<tlm_name>". These will instead be assumed to be in the format "<component_name>-<tlm_name>".

    Arguments:
        tlm_id (str): Unique telemetry point ID. Format: "[[<packet_name>-]<component_name>-]<tlm_name>".

    Returns:
        component_name (str): Name of the component from which the telemetry point was obtained.
            NOTE: Will return None if the telemetry point name is not unique to a component.
        packet_name (str): Name of the component from which the telemetry point was obtained.
            NOTE: Will return None if the telemetry point name is not unique to a packet.
        tlm_name (str): Name of the telemetry point.
    """
    if "-" in tlm_id:
        pieces = tlm_id.split("-")
        if len(pieces) == 2:
            # TODO: rework to handle telemetry that's unique to packet but not component (see corresponding test)
            component_name, tlm_name = pieces
            packet_name = None
        elif len(pieces) == 3:
            packet_name, component_name, tlm_name = pieces
        else:
            raise ValueError
    else:
        tlm_name = tlm_id
        component_name = None
        packet_name = None
    return component_name, packet_name, tlm_name


def get_tlm_id(
        tlm_name: str,
        packet: Optional[str] = None,
        board: Optional[str] = None,
        stack: Optional[str] = None,
        component: Optional[str] = None,
        use_component_prefix: Optional[bool] = True,
) -> str:
    """Get the telemetry ID for a given telemetry point, based on the packet and component (or stack & board in place of component).
    Format: "[[<packet_name>-]<component_name>-]<tlm_name>" or equivalently "[[<packet_name>-]<board_name>_<stack_name>-]<tlm_name>".

    Arguments:
        tlm_name (str): Name of the telemetry point for which the ID is requested.
        packet (str, default=None): Optional. Name of the packet from which the telemetry point is obtained.
            NOTE: If a packet name is provided, it is assumed that the packet prefix should be included.
        board (str, default=None): Optional. Name of the board from which the telemetry point is obtained.
        stack (str, default=None): Optional. Name of the spacecraft stack from which the telemetry point is obtained.
        component (str, default=None): Optional. Name of the component (board & stack combined) from which the telemetry point is obtained.
        use_component_prefix (bool, default=True): Whether the requested telemetry point uses a component prefix in its ID.

    Returns:
        tlm_id (str): Telemetry ID of the telemetry point for the given component (if specified).

    Raises:
        ValueError if insufficient information is provided to create the telemetry ID.
    """
    # Handle packet uniqueness
    if packet:
        packet_section = f"{packet}-"
    else:
        packet_section = ""

    # Handle component uniqueness
    if use_component_prefix:
        if not any([board, stack, component]):
            raise ValueError(
                f"Prefix requested but insufficient information provided (board={board}, stack={stack}, component={component})."
            )
        elif component and not (board and stack):
            component_section = f"{component}-"
        elif (board and stack) and not component:
            component_section = f"{board}_{stack}-"
        elif all([component, board, stack]):
            if component != f"{board}_{stack}":
                raise ValueError(
                    f"Component, stack, and board were provided but are not compatible (board={board}, stack={stack}, component={component})."
                )
            else:
                component_section = f"{component}-"
    else:
        component_section = ""

    tlm_id = f"{packet_section}{component_section}{tlm_name}"

    return tlm_id


def get_telemetry_ids_required(
        soh_checks: dict, systems_to_check: list[str]
) -> tuple[set[str], set[str]]:
    """Get the unique telemetry IDs required to conduct the state of health checks for the given list of subsystems.

    Arguments:
        soh_checks (dict): State of health (SOH) check specifications.
        systems_to_check (list of strings): List of system names within the state of health specifications to be checked.

    Returns:
        latest_tlm_ids (set of strings): List of telemetry point IDs for which only the latest telemetry data needs to be queried.
        historical_tlm_ids (set of strings): List of telemetry point IDs for which historical telemetry data needs to be queried.
    """
    latest_tlm_ids: set[str] = set()
    historical_tlm_ids: set[str] = set()
    primary_stack = soh_checks["metadata"]["primary_stack"]

    for system in systems_to_check:
        system_checks = soh_checks["SOH"][system]["checks"]
        for check in system_checks:
            tlm_point_name = f"{check['tlm_point']}"

            # get the tlm_ids we need
            if check["check_type"] == "clock-latest":
                # MVP - query all possible packets for the RTC_TIME
                # TODO: Come up with a more elegant solution here
                allowable_packets = check["check_params"]
                tlm_point_ids = [
                    get_tlm_id(
                        tlm_name=tlm_point_name,
                        packet=packet,
                        board=check["board"],
                        stack=primary_stack,
                        use_component_prefix=check["use_stack_prefix"],
                    )
                    for packet in allowable_packets
                ]
            else:
                tlm_point_ids = [
                    get_tlm_id(
                        tlm_name=tlm_point_name,
                        board=check["board"],
                        stack=primary_stack,
                        use_component_prefix=check["use_stack_prefix"],
                    ),
                ]

            # add to the appropriate set
            if check["check_type"] == "non-static":
                historical_tlm_ids.update(tlm_point_ids)
            else:
                latest_tlm_ids.update(tlm_point_ids)

    return latest_tlm_ids, historical_tlm_ids


def group_telemetry_points_by_component(
        tlm_id_list: Union[list[str], set[str]],
) -> dict[str, list[str]]:
    """Group a list of unique telemetry IDs by component. Used to increase query speed for requesting historical data.

    Arguments:
        tlm_id_list (list of strings)

    Returns:
        grouped_telemetry (dict): Dictionary with component names as keys, and lists of associated telemetry names as values.
    """
    # return a dictionary with component as key and list of tlm_names for that comopnent as values
    grouped_telemetry: dict = {}
    for tlm_id in tlm_id_list:
        tlm_component, _, tlm_name = get_component_packet_and_point_name_from_id(tlm_id)
        if tlm_component in grouped_telemetry:
            grouped_telemetry[tlm_component].append(tlm_name)
        else:
            grouped_telemetry[tlm_component] = [tlm_name]
    return grouped_telemetry


def parse_SOH_config() -> dict:
    """Read in the state of health (SOH) check configuration file (/autohealthcheck/config/SOH_config.toml).

    Arguments: None.

    Returns:
        soh_checks (dict): Dictionary representation of the state of health checks.
            Structure:
                soh_checks = {
                    'metadata': {<check metadata - see file>},
                    'SOH': {
                        '<system_1>': {
                            'checks': [<list of checks>]
                        },
                        '<system_2>': {
                            'checks': [<list of checks>]
                        },
                        ...
                    }
                }
    """
    soh_checks = toml.load(
        os.path.join(os.path.dirname(__file__), "config", "SOH_config.toml")
    )
    return soh_checks


def parse_FDIR_config() -> dict:
    """Read in the fault detection, isolation, and recovery flag (FDIR) configuration file (autohealthcheck/config/FDIR_config.toml).

    Arguments: None.

    Returns:
        fdir_checks (dict): Dictionary representation of the FDIR checks, with unspecified values padded with empty entries.
            Structure:
                fdir_checks = {
                    'metadata': {<check metadata - see file>},
                    'checks': {
                        '<system_1>': {
                            'primary' {
                                'APC': {
                                    'base_range': [<min index>, <max index>],
                                    'ignore_ranges': [<series of index ranges to ignore>]
                                },
                                'FC': {
                                    'base_range': [<min index>, <max index>],
                                    'ignore_ranges': [<series of index ranges to ignore>]
                                },
                            },
                            'secondary' {
                                'APC': {
                                    'base_range': [<min index>, <max index>],
                                    'ignore_ranges': [<series of index ranges to ignore>]
                                },
                                'FC': {
                                    'base_range': [<min index>, <max index>],
                                    'ignore_ranges': [<series of index ranges to ignore>]
                                },
                            }
                        }
                        },
                        '<system_1>': {<system check details - as above>}
                        },
                        ...
                    }
                }
    """
    fdir_checks = toml.load(
        os.path.join(os.path.dirname(__file__), "config", "FDIR_config.toml")
    )

    systems_to_check = list(fdir_checks["checks"].keys())

    # pad out the optional entries so the structure is nicer to work with
    for system in systems_to_check:
        for stack in ["primary", "secondary"]:
            if stack not in fdir_checks["checks"][system]:
                fdir_checks["checks"][system][stack] = {}
            for board in ["APC", "FC"]:
                if board not in fdir_checks["checks"][system][stack]:
                    fdir_checks["checks"][system][stack][board] = {}
                for range_key in ["base_range", "ignore_ranges"]:
                    if range_key not in fdir_checks["checks"][system][stack][board]:
                        fdir_checks["checks"][system][stack][board][range_key] = []
    return fdir_checks


def create_FDIR_index_list(
        base_range: list[int], ignore_ranges: list[list[int]]
) -> list[int]:
    """Get the list of valid indices to request implied by the given base range and ranges of indices to ignore.

    Arguments:
        base_range (list): Sorted 'base' range of indices, from which the range of indices to be ignored will be removed.
            NOTE: An empty list is also accepted, implying that there are no valid indices to be requested.
        ignore_ranges (list of lists): List of lists, specifying indices to be removed from the base range.
            Two formats are pemitted: Single-element lists will exclude a single index (e.g. [17]), while two-element lists
                will exclude the range from the first element to the second element, inclusive of both endpoints (e.g. [18, 23]).

    Returns:
        index_list (list of ints): List of valid indices to request implied by the given base range and ranges of indices to ignore.

    Raises:
        ValueError if:
            -The base_range specification is either not empty ([]) or not a range in the format [<min_index>, <max_index>].
            -Any ignore range is not in one of the two valid formats.
            -Any two-element ignore range has elements in the incorrect order (i.e. fails when ignore_range[0] > ignore_range[1]).
            -Any base_range or ignore range subrange contains a negative index.
    """
    is_valid_range = lambda interval: (interval[0] <= interval[1]) and (
        all(el >= 0 for el in interval)
    )
    is_valid_nonempty_base_range = len(base_range) == 2 and is_valid_range(base_range)

    if is_valid_nonempty_base_range:
        base_range_indices = set(i for i in range(base_range[0], base_range[1] + 1))

        # create set of indices to ignore
        ignore_indices = set()
        for ignore_range in ignore_ranges:
            if (
                    len(ignore_range) == 1 and ignore_range[0] >= 0
            ):  # single index to ignore
                ignore_indices.add(ignore_range[0])
            elif len(ignore_range) == 2 and is_valid_range(
                    ignore_range
            ):  # range to ignore
                ignore_indices.update(
                    [i for i in range(ignore_range[0], ignore_range[1] + 1)]
                )
            else:
                raise ValueError(f"Ivalid ignore range specification: {ignore_range}.")

        index_list = list(base_range_indices.difference(ignore_indices))
    elif base_range == []:
        index_list = []
    else:
        raise ValueError(
            "Base range must be in the format [<min_index>, <max_index>] or an empty list (no range)."
        )
    return index_list


def get_stack_name(primary_stack: str, stack_priority: str) -> str:
    """Get the name of a stack, given the vehicle's current primary stack and the specified stack priority.
    e.g. if the primary stack is 'YM', the 'primary' stack is 'YM' and the secondary stack is 'YP'.

    Arguments:
        primary_stack (str): Name of the current primary stack.
        stack_priority (str): Priority of the stack name being sought, must be one of 'primary' or 'secondary' (case-insensitive).

    Returns:
        stack_name (str): Name of the requested stack.

    Raises:
        ValueError if:
            -The given primary_stack name is not a valid stack.
            -The given stack_priority is not one of 'primary' or 'secondary' (case-insensitive).
    """
    stack_priority = stack_priority.lower()
    primary_stack = primary_stack.upper()

    POSSIBLE_STACKS = Stack.list()

    if primary_stack not in POSSIBLE_STACKS:
        raise ValueError(
            f"Given primary stack '{primary_stack}' is not a valid stack name. Stack names must be one of {POSSIBLE_STACKS}."
        )

    if stack_priority == "primary":
        stack_name = primary_stack
    elif stack_priority == "secondary":
        stack_name = set(POSSIBLE_STACKS).difference({primary_stack}).pop()
    else:
        raise ValueError(f"Given stack priority '{stack_priority}' is invalid.")
    return stack_name


def request_SOH_telemetry(
        client: Client, soh_checks: dict
) -> dict[
    str, list[Union[TelemetryLatestRepresentation, TelemetryHistoryRepresentation]]
]:
    """Retrieve the telemetry data necessary to conduct the specified state of health checks from the Scepter API.

    Arguments:
        client (scepter.wrapper.Client instance): Scepter API client, used to query for telemetry.
        soh_checks (dict): State of health (SOH) check definitions.

    Returns:
        soh_telemetry (dict): Telemetry required to conduct state of health checks. For checks which only require a single
            telemetry point, only the latest telemetry point is retrieved. For checks requiring multiple telemetry points,
            a selection of the latest data is retrieved - with earliest timestamp being the non_static_time_buffer_seconds
            before the oldest observed_at time for historical data.
            Structure:
                soh_telemetry = {
                    <telemetry_id_1>: [<telemetry data>],
                    <telemetry_id_2>: [<telemetry data>],
                    ...
                }
    """
    systems_to_check = list(soh_checks["SOH"].keys())

    latest_tlm_ids, historical_tlm_ids = get_telemetry_ids_required(
        soh_checks, systems_to_check
    )

    all_tlm_ids = latest_tlm_ids.union(historical_tlm_ids)

    # get latest telemetry
    print("Requesting latest telemetry...")
    add_to_output_text("Requesting latest telemetry...")
    soh_telemetry_list = client.get_telemetry_latest(tlm_id=all_tlm_ids)

    # organise into a nice structure where the key is the telemetry name and the value is the observation
    soh_telemetry: dict[
        str, list[Union[TelemetryLatestRepresentation, TelemetryHistoryRepresentation]]
    ] = {
        telemetry_item.tlm_id: [telemetry_item] for telemetry_item in soh_telemetry_list
    }

    # get additional telemetry for items that require a non-static check
    print("Requesting historical telemetry...")
    add_to_output_text("Requesting historical telemetry...")
    grouped_historical_tlm = group_telemetry_points_by_component(historical_tlm_ids)
    for historical_tlm_component in grouped_historical_tlm:
        historical_tlm_names = grouped_historical_tlm[historical_tlm_component]

        # get the oldest observed_at time and use that as the basis for the grouped query
        oldest_observed_at_time = datetime.utcnow()
        for historical_tlm_name in historical_tlm_names:
            historical_tlm_id = get_tlm_id(
                component=historical_tlm_component,
                tlm_name=historical_tlm_name,
                use_component_prefix=True,
            )
            oldest_observed_at_time = min(  # type: ignore
                oldest_observed_at_time, soh_telemetry[historical_tlm_id][0].observed_at
            )

        group_query_start_time = oldest_observed_at_time - timedelta(
            seconds=soh_checks["metadata"]["non_static_time_buffer_seconds"]
        )

        historical_data = client.get_telemetry_history(
            component=historical_tlm_component,
            tlm_name=historical_tlm_names,
            observed_at_start=group_query_start_time,
        )

        # add in the historical data
        for tlm_item in historical_data:
            soh_telemetry[tlm_item.tlm_id].append(tlm_item)

    # remove duplicate points & make sure it's sorted properly
    for historical_tlm_id in historical_tlm_ids:
        if (len(soh_telemetry[historical_tlm_id]) > 1) and (
                soh_telemetry[historical_tlm_id][0].observed_at
                == soh_telemetry[historical_tlm_id][1].observed_at
        ):
            _ = soh_telemetry[historical_tlm_id].pop(0)

        soh_telemetry[historical_tlm_id] = sorted(
            soh_telemetry[historical_tlm_id],
            key=lambda x: x.observed_at,  # type: ignore
            reverse=True,
        )

    # group clock telemetry into a single "RTC_TIME" key
    clock_telemetry = [
        soh_telemetry[tlm_id][0]
        for tlm_id in soh_telemetry
        if tlm_id.endswith("RTC_TIME")
    ]
    soh_telemetry["RTC_TIME"] = sorted(
        clock_telemetry, key=lambda x: x.observed_at, reverse=True  # type: ignore
    )

    return soh_telemetry


def request_FDIR_telemetry(
        client: Client, primary_stack: str
) -> tuple[list[TelemetryLatestRepresentation], dict[str, Optional[datetime]]]:
    """Retrieve the FDIR slim packet telemetry from the Scepter API.

    Arguments:
        client (scepter.wrapper.Client instance): Scepter API client, used to query for telemetry.
        primary_stack (str): Primary stack of the spacecraft.

    Returns:
        fdir_telemetry (list of scepter.wrapper.TelemetryLatestRepresentation instances): List of telemetry items obtained
            from the FDIR slim packets for both stacks and boards.
        packet_times (dict): Dictionary containing the packet name as keys and observed_at time as values.
    """
    fdir_telemetry = []
    packet_times = {}

    for board in ["APC", "FC"]:
        fdir_packet_name = f"FDIR_SLIM_TLM_{board}"
        telemetry = client.get_telemetry_latest(packet=fdir_packet_name)
        fdir_telemetry += telemetry

        for item in telemetry:
            if item.tlm_id.startswith(f"{board}_{primary_stack}"):
                fdir_packet_time = item.observed_at
                packet_times[fdir_packet_name] = fdir_packet_time
                break

    return fdir_telemetry, packet_times


def process_FDIR_packets(
        fdir_telemetry_list: list[TelemetryLatestRepresentation],
) -> dict:
    """Process the raw FDIR telemetry into a

    Arguments:
        fdir_telemetry (list of scepter.wrapper.TelemetryLatestRepresentation instances): List of telemetry items obtained
            from the FDIR slim packets for both stacks and boards.

    Returns:
        processed_data (dict): Dictionary of FDIR telemetry arranged for score aggregation.
            Structure:
                processed_data = {
                    "YM": {
                        "APC": {
                            '<tlm_name_apc_ym_1>': <telemetry>,
                            '<tlm_name_apc_ym_2>': <telemetry>,
                            ...
                        },
                        "FC": {
                            '<tlm_name_fc_ym_1>': <telemetry>,
                            '<tlm_name_fc_ym_2>': <telemetry>,
                            ...
                        },
                    },
                    "YP": {
                        "APC": {
                            '<tlm_name_apc_yp_1>': <telemetry>,
                            '<tlm_name_apc_yp_2>': <telemetry>,
                            ...
                        },
                        "FC": {
                            '<tlm_name_fc_yp_1>': <telemetry>,
                            '<tlm_name_fc_yp_2>': <telemetry>,
                            ...
                        },
                    },
                }

    Raises:
        RuntimeError if telemetry is returned in an unexpected format.
    """
    processed_data: dict = {
        stack_name: {board: {} for board in ["APC", "FC"]}
        for stack_name in Stack.list()
    }

    for tlm_item in fdir_telemetry_list:
        try:
            component, _, tlm_name = get_component_packet_and_point_name_from_id(
                tlm_item.tlm_id
            )
            if component is not None:
                board, stack_name = component.split("_")
                processed_data[stack_name][board][tlm_name] = tlm_item
            else:
                raise ValueError
        except:
            if tlm_item.tlm_id.startswith("FDIR_SLIM_TLM"):
                _, component, tlm_name = tlm_item.tlm_id.split("-")
                board, stack_name = component.split("_")
                processed_data[stack_name][board][tlm_name] = tlm_item
            else:
                raise RuntimeError(f"Failure inserting telemetry ID: {tlm_item.tlm_id}")
    return processed_data


def calculate_FDIR_scores(fdir_checks: dict, fdir_data: dict) -> dict:
    """Calculate aggregated FDIR flag counts for each system and FDIR status.

    Arguments:
        fdir_checks (dict): FDIR checks defined by the configuration file.
        fdir_data (dict): Dictionary of FDIR telemetry arranged for score aggregation (see process_FDIR_packets()).

    Returns:
        fdir_scores (dict): Aggregated FDIR scores grouped by system and FDIR status.
            Structure:
                fdir_scores = {
                    '<system_1>': {
                        'boards_required': [<sorted list of required boards for checks (APC or FC)>],
                        'NOT_MANAGED_OR_INACTIVE': {
                            'score': <system flag score>,
                            'flags': [<list of telemetry IDs>]
                            },
                        'OK': {
                            'score': <system flag score>,
                            'flags': [<list of telemetry IDs>]
                            },
                        'YELLOW': {
                            'score': <system flag score>,
                            'flags': [<list of telemetry IDs>]
                            },
                        'RED': {
                            'score': <system flag score>,
                            'flags': [<list of telemetry IDs>]
                            },
                        'NO_STATUS': {
                            'score': <system flag score>,
                            'flags': [<list of telemetry IDs>]
                            },
                    },
                    '<system_2>': {...}
                    },
                    ...
                }
    """
    primary_stack = fdir_checks["metadata"]["primary_stack"]
    systems_to_check = list(fdir_checks["checks"].keys())

    fdir_scores: dict = {}
    for system in systems_to_check:
        system_scores: dict = {
            flag_status: {"score": 0, "flags": []} for flag_status in FDIRStatus.list()
        }
        system_scores[
            "boards_required"
        ] = (
            set()
        )  # TODO: This can be taken directly from the fdir_checks definitions - would be cleaner
        system_prefix = fdir_checks["checks"][system]["prefix"]

        for stack_priority in ["primary", "secondary"]:
            stack_name = get_stack_name(primary_stack, stack_priority)
            for board in ["APC", "FC"]:
                subcheck = fdir_checks["checks"][system][stack_priority][board]

                index_list = create_FDIR_index_list(
                    subcheck["base_range"], subcheck["ignore_ranges"]
                )
                if len(index_list) > 0:
                    system_scores["boards_required"].add(board)

                for index in index_list:
                    tlm_name = f"{system_prefix}{index:03d}"
                    tlm_id = get_tlm_id(
                        tlm_name=tlm_name, stack=stack_name, board=board
                    )
                    tlm_item = fdir_data[stack_name][board][tlm_name]

                    try:
                        system_scores[tlm_item.value]["score"] += 1
                        system_scores[tlm_item.value]["flags"].append(tlm_id)
                    except KeyError:  # null telemetry - mark as NO_STATUS
                        system_scores[FDIRStatus.NO_STATUS.value]["score"] += 1
                        system_scores[FDIRStatus.NO_STATUS.value]["flags"].append(
                            tlm_id
                        )

        system_scores["boards_required"] = sorted(
            list(system_scores["boards_required"])
        )  # for JSON serialisation
        fdir_scores[system] = system_scores

    return fdir_scores


def check_individual_item(
        check: dict, soh_telemetry: dict, metadata: dict
) -> tuple[
    CheckStatus,
    str,
    list[Union[TelemetryLatestRepresentation, TelemetryHistoryRepresentation]],
]:
    """Conduct an individual health check. Checks can be one of five (5) supported types:
        1) 'exact-value': Check PASS if the value is exactly equal to the specified target value.
        2) 'range-exclusive': Check PASS if min_value < received_value < max_value.
        3) 'range-inclusive': Check PASS if min_value <= received_value <= max_value.
        4) 'non-static': Check PASS if the value is non-static (i.e. changing with time). Check is INCOMPLETE if not enough data is available.
        5) 'watchdog': Check PASS if the value is within specified margin of the specified target.
        6) 'clock-latest': Check PASS if the observed_at and received_at time of the latest telemetry from a given subset are distinct and not too far apart.
    NOTE: All check types will be marked as INCOMPLETE if the available telemetry data is too far out of date.

    Arguments:
        check (dict): Check definition.
        soh_telemetry (dict): Telemetry data needed to conduct state of health checks.
        metadata (dict): Metadata for the current state of health checks, as defined in the config file.

    Returns:
        check_status (CheckStatus): Result of the check performed.
        status_justification (str): Text describing reason for given check_status.
        received_tlm_point_data (scepter.schemas.{TelemetryLatestRepresentation, TelemetryHistoryRepresentation}): Telemetry
            data associated with the current check.

    Raises:
        ValueError if the given check type is invalid/unsupported.
    """
    FAIL_REASONS = {
        "exact-value": "Does not match expected value.",
        "range-exclusive": "Value outside expected range.",
        "range-inclusive": "Value outside expected range.",
        "non-static": "Value remained constant for 2 or more successive times.",
        "watchdog": "Timer not within buffer of expected value.",
        "clock-latest": "Observed_at and received_at times were equal or too far apart.",
    }
    POSSIBLE_CHECK_TYPES = list(FAIL_REASONS.keys())

    check_type = check["check_type"]
    required_tlm_point = check["tlm_point"]
    required_tlm_point_board = check["board"]

    if check_type.lower() not in POSSIBLE_CHECK_TYPES:
        raise ValueError(f"Unsupported check type '{check_type}'.")

    if check_type == "clock-latest":
        required_tlm_point_id = "RTC_TIME"
    else:
        required_tlm_point_id = get_tlm_id(
            tlm_name=required_tlm_point,
            board=required_tlm_point_board,
            stack=metadata["primary_stack"],
            use_component_prefix=check["use_stack_prefix"],
        )

    received_tlm_point_data = soh_telemetry[required_tlm_point_id]

    try:
        # Incomplete result if insufficient data to perform the check
        # TODO: Rework this to be more readable/cleaner
        if (
                len(received_tlm_point_data) == 0
                or (
                datetime.utcnow() - received_tlm_point_data[0].observed_at
        ).total_seconds()
                > metadata["latest_tlm_time_buffer_seconds"]
        ):
            check_status = CheckStatus.INCOMPLETE
            status_justification = "Recent data not available."

        # If sufficient data, conduct the check
        elif check_type == "exact-value":
            tlm_point_value = received_tlm_point_data[0].value
            expected_value = check["check_params"][0]
            check_status = (
                CheckStatus.PASS
                if tlm_point_value == expected_value
                else CheckStatus.FAIL
            )
        elif check_type == "range-exclusive":
            tlm_point_value = received_tlm_point_data[0].value
            min_allowed, max_allowed = check["check_params"]
            check_status = (
                CheckStatus.PASS
                if min_allowed < tlm_point_value < max_allowed
                else CheckStatus.FAIL
            )
        elif check_type == "range-inclusive":
            tlm_point_value = received_tlm_point_data[0].value
            min_allowed, max_allowed = check["check_params"]
            check_status = (
                CheckStatus.PASS
                if min_allowed <= tlm_point_value <= max_allowed
                else CheckStatus.FAIL
            )
        elif check_type == "non-static":
            if len(received_tlm_point_data) > 1:
                check_status = (
                    CheckStatus.PASS
                    if received_tlm_point_data[0].value
                       != received_tlm_point_data[1].value
                    else CheckStatus.FAIL
                )
            else:
                check_status = CheckStatus.INCOMPLETE
                status_justification = "Recent data not available."

        elif check_type == "watchdog":
            wdt_buffer_seconds = metadata["watchdog_timer_buffer_seconds"]
            wdt_target_value = check["check_params"][0]
            wdt_value = received_tlm_point_data[0].value

            wdt_delta = abs(wdt_target_value - wdt_value)
            check_status = (
                CheckStatus.PASS
                if wdt_delta <= wdt_buffer_seconds
                else CheckStatus.FAIL
            )
        elif check_type == "clock-latest":
            latest_data = received_tlm_point_data[0]
            obs_rec_time_delta = (
                    latest_data.received_at - latest_data.observed_at
            ).total_seconds()

            clock_delta_within_bounds = (
                    0 < obs_rec_time_delta <= metadata["clock_max_time_buffer_seconds"]
            )

            check_status = (
                CheckStatus.PASS if clock_delta_within_bounds else CheckStatus.FAIL
            )
        else:
            raise ValueError(f"Unsupported check type '{check_type}'.")
    except:
        check_status = CheckStatus.ERROR
        status_justification = "An error occurred while conducting the check."

    if check_status == CheckStatus.FAIL:
        status_justification = FAIL_REASONS[check_type]
    elif check_status == CheckStatus.PASS:
        status_justification = ""

    return check_status, status_justification, received_tlm_point_data


def perform_SOH_checks(soh_checks: dict, soh_telemetry: dict) -> dict[str, list]:
    """Perform the state of health checks, by comparing the requested telemetry data with the defined check parameters.
    NOTE: Currently this function also displays the check results to console, but this will be decoupled in future.

    Arguments:
        soh_checks (dict): Definitions of state of health checks to conduct.
        soh_telemetry (dict): Latest data for the telemetry points required to conduct state of health checks.

    Returns:
        soh_results (dict): Summary of check results.
    """
    systems_to_check = list(soh_checks["SOH"].keys())

    start_time = datetime.utcnow()
    soh_results: dict[str, list] = {system_name: [] for system_name in systems_to_check}

    for system_name in systems_to_check:
        for check in soh_checks["SOH"][system_name]["checks"]:
            (
                check_status,
                status_justification,
                received_tlm_point_data,
            ) = check_individual_item(
                check, soh_telemetry, metadata=soh_checks["metadata"]
            )

            tlm_point_value = received_tlm_point_data[0].value
            seconds_since_observed = round(
                (start_time - received_tlm_point_data[0].observed_at).total_seconds()  # type: ignore
            )

            soh_results[system_name].append(
                {
                    "check": check,
                    "tlm_value": tlm_point_value,
                    "tlm_age_seconds": seconds_since_observed,
                    "check_status": check_status,
                    "status_justification": status_justification,
                }
            )

    return soh_results


def check_FDIR_packet_times(fdir_checks: dict, fdir_packet_times: dict) -> bool:
    """Produce a printout showing the FDIR packet observed_at times, and display a warning to the operator if they are not
    sufficiently recent.

    Arguments:
        fdir_checks (dict): FDIR checks defined by the configuration file.
        fdir_packet_times (dict): Dictionary containing the packet name as keys and observed_at time as values.

    Returns: None.
    """
    both_packets_recent = True

    for packet in fdir_packet_times:
        packet_time = fdir_packet_times[packet]
        packet_time_delta = (datetime.utcnow() - packet_time).total_seconds()
        packet_is_recent = (
                packet_time_delta
                <= fdir_checks["metadata"]["latest_tlm_time_buffer_seconds"]
        )
        if packet_is_recent:
            status_colour = Fore.GREEN
        else:
            status_colour = Fore.CYAN
            both_packets_recent = False

        print(
            f"{packet:<17s} received at {packet_time.isoformat()} ({status_colour}{round(packet_time_delta)}s old{Fore.RESET})"
        )
        add_to_output_text(f"{packet:<17s} received at {packet_time.isoformat()} ({round(packet_time_delta)}s old)")

    if not both_packets_recent:
        print(
            f"{Fore.YELLOW}Warning: Recent FDIR telemetry has not been received for both boards.{Fore.RESET}"
        )
        add_to_output_text( f"Warning: Recent FDIR telemetry has not been received for both boards.")
        return False
    return True


def load_previous_FDIR_scores(quindar_client: QClient) -> dict:
    """Load the previous FDIR check scores from file for comparison with current check results.

    Arguments: None.

    Returns:
        prev_results (dict): Dictionary representation of the previous FDIR score check results
        or {} if:
            1) No previous results file exists (FileNotFoundError).
            2) The previous results file exists, but is in an old format (KeyError).
    """
    spacecraft_client = Spacecrafts(api_client=quindar_client)
    sc_params_client = SpacecraftParameters(api_client=quindar_client)
    filters = {'name': {'_eq': SPACECRAFT_NAME}}
    spacecraft = spacecraft_client.get(filters=filters).response[0]
    try:
        sc_param = sc_params_client.get_latest_value(spacecraft_id=spacecraft.id, parameter_name='PREVIOUS_FDIR_SCORE')
        print(sc_param)
    except QApiError:
        print(f"No param setup for PREVIOUS_FDIR_SCORE on {spacecraft.name}")
        return {}
    print(sc_param)
    prev_results = json.loads(sc_param.value)

    # check results file version number (NOTE: original format doesn't have this key)
    file_format_version_number = prev_results["check_metadata"][
        "result_version_number"
    ]
    if (
            file_format_version_number == "1.1"
    ):  # missing "NO_STATUS" flag, so add that in
        for system in prev_results["results"]:
            prev_results["results"][system][FDIRStatus.NO_STATUS.value] = {
                "score": 0,
                "flags": [],
            }
    return prev_results
    #
    # except FileNotFoundError:  # no existing results file
    #     prev_results = {}
    # except KeyError:  # existing results file but old format (pre-version numbers)
    #     prev_results = {}
    # return prev_results


def save_FDIR_scores(fdir_checks: dict, fdir_results: dict, fdir_packet_times, quindar_client: QClient) -> None:
    """Save the results from the most recent check of FDIR flags, for later comparison.

    Arguments:
        fdir_checks (dict): FDIR flag check definitions and metadata.
        fdir_results (dict): FDIR score results from checking the current FDIR flag definitions.
        fdir_packet_times (dict): Dictionary containing the packet name as keys and observed_at time as values.

    Returns: None.

    Creates:
        previous_FDIR_scores.json: JSON representation of current FDIR scores for later comparison.

    FDIR Savefile (previous_FDIR_results.json) changelog:
        -v0 (no version number): Only stores scores for each system and status.
        -v1.0: Stores scores along with flags responsible for score for each system and status.
        -v1.1: Add in a boards_required key for each system, containing a list of which boards are used in conducting checks.
    """
    results_to_save = {
        "check_time": datetime.utcnow().strftime(ISO_DATETIME_FORMAT),
        "check_metadata": fdir_checks["metadata"],
        "results": fdir_results,
    }

    # additional info to include
    results_to_save["check_metadata"][
        "result_version_number"
    ] = "1.2"  # includes flags & boards_required + a NO_STATUS status for FDIR flags

    for packet_name in fdir_packet_times:
        results_to_save["check_metadata"][
            f"packet_{packet_name}_observed"
        ] = fdir_packet_times[packet_name].strftime(ISO_DATETIME_FORMAT)

    # with open(
    #         os.path.join(os.path.dirname(__file__), "previous_FDIR_scores.json"), "w"
    # ) as score_file:
    #     score_file.write(json.dumps(results_to_save, indent=True))
    spacecraft_client = Spacecrafts(api_client=quindar_client)
    sc_params_client = SpacecraftParameters(api_client=quindar_client)
    filters = {'name': {'_eq': SPACECRAFT_NAME}}
    spacecraft = spacecraft_client.get(filters=filters).response[0]
    sc_params_client.post(SpacecraftParameter(
        spacecraft_id=spacecraft.id,
        name="PREVIOUS_FDIR_SCORE",
        value=json.dumps(results_to_save)
    ))


def get_FDIR_score_deltas(
        fdir_checks: dict, previous_fdir_results: dict, current_fdir_results: dict
) -> dict:
    """Calculate the score difference between current and previous results for each subsystem and result category of FDIR flags.

    Arguments:
        fdir_checks (dict): Specification of FDIR flags to check, based on the parsed FDIR config file.
        previous_fdir_results (dict): Results from the last time the FDIR flags were checked.
            NOTE: These results may not necessarily be for the same checks, so caution should be taken when interpreting score deltas.
            NOTE: If no previous results are available, all score deltas will be set to 0 and a notification will be printed.
        current_fdir_results (dict): Results from the most recent FDIR flag check, using the checks defined in fdir_checks.

    Returns:
        score_deltas (dict): Dictionary representation of the score differences between previous and current FDIR results.
            NOTE: score_delta = current_result - previous_result
            Structure:
                score_deltas = {
                    'transitions': {
                        '<flag name 1>': {
                            'changed_from': <FDIRStatus>
                            'changed_to': <FDIRStatus>
                        },
                        ...
                    },
                    '<system_1>': {
                        'NOT_MANAGED_OR_INACTIVE': <score delta>,
                        'OK': <score delta>,
                        'YELLOW': <score delta>,
                        'RED': <score delta>,
                        'NO_STATUS': <score delta>,
                    },
                    '<system_2>': {
                        'NOT_MANAGED_OR_INACTIVE': <score delta>,
                        'OK': <score delta>,
                        'YELLOW': <score delta>,
                        'RED': <score delta>,
                        'NO_STATUS': <score delta>,
                    },
                    ...
                }

    Raises:
        ValueError if a system to be checked in the current check was not included in the previous FDIR score check.
        Warning (printed) if the previous FDIR score results are more than prev_check_time_buffer_seconds out of date.
    """

    # if no previous results, set all deltas to 0 and set previous check time to datetime.min
    if not previous_fdir_results:
        previous_fdir_results["results"] = current_fdir_results
        previous_fdir_results["check_time"] = "0001-01-01T00:00:00Z"
        print(
            f"{Fore.BLUE}No previous FDIR score results file found - creating new results file.{Fore.RESET}"
        )
        add_to_output_text(
            f"No previous FDIR score results file found - creating new results file.")

    score_deltas: dict = {
        system: {status: 0 for status in FDIRStatus.list()}
        for system in current_fdir_results
    }
    score_deltas["transitions"] = {}

    previous_check_time = datetime.strptime(
        previous_fdir_results["check_time"], ISO_DATETIME_FORMAT
    )

    if (datetime.utcnow() - previous_check_time).total_seconds() > fdir_checks[
        "metadata"
    ]["prev_check_time_buffer_seconds"]:
        print(
            f"{Fore.YELLOW}Warning: Previous FDIR flag scores are out of date - please perform FDIR checks manually for this pass.{Fore.RESET}"
        )
        add_to_output_text(
            f"Warning: Previous FDIR flag scores are out of date - please perform FDIR checks manually for this pass.")

    for system in current_fdir_results:
        if system in previous_fdir_results["results"]:
            for status in FDIRStatus.list():
                current_flags = set(current_fdir_results[system][status]["flags"])
                previous_flags = set(
                    previous_fdir_results["results"][system][status]["flags"]
                )

                increases = current_flags.difference(
                    previous_flags
                )  # flags in current_flags that are not in previous_flags
                decreases = previous_flags.difference(
                    current_flags
                )  # flags in previous_flags that are not in current_flags

                # Calculate transitions & deltas
                score_deltas[system][status] = len(current_flags) - len(previous_flags)

                # Map transitions
                for flag in increases:
                    if not score_deltas["transitions"].get(flag):
                        score_deltas["transitions"][flag] = {  # type: ignore
                            "changed_from": None,
                            "changed_to": status,
                        }
                    else:
                        score_deltas["transitions"][flag]["changed_to"] = status  # type: ignore

                for flag in decreases:
                    if not score_deltas["transitions"].get(flag):
                        score_deltas["transitions"][flag] = {  # type: ignore
                            "changed_from": status,
                            "changed_to": None,
                        }
                    else:
                        score_deltas["transitions"][flag]["changed_from"] = status  # type: ignore

        else:
            # TODO: handle better (case where checks change or a previous FDIR check has not yet been conducted - no FDIR file exists)
            raise ValueError(
                f"System '{system}' was not recorded in previous FDIR score check."
            )
    return score_deltas


def get_check_status_colour(check_status: CheckStatus) -> str:
    """Get the display colour corresponding to a given check result status.

    Arguments:
        check_status (CheckStatus): Status of a health check.

    Returns:
        status_colour (str): Print colour used when displaying the check_status.
    """
    CHECK_STATUS_COLOURS = {
        "PASSED": "green",
        "FAILED": "bright_red",
        "WARNING": "bright_yellow",
        "INCOMPLETE": "cyan",
    }
    return CHECK_STATUS_COLOURS[check_status.value]


def get_SOH_results_table(soh_results: dict, display_options: dict) -> Table:
    """Create a Table representing the results from the set of state of health checks.

    Arguments:
        soh_results (dict): Summary of state of health checks and results.
        display_options (dict, default = None): Printout display options - see main() for further details.

    Returns:
        soh_table (rich.table.Table): Tabular representation of state of health check results.
    """
    columns = [
        Column(header="ID", justify="center"),
        Column(header="System"),
        Column(header="Tlm Point", no_wrap=True),
        Column(header="Value", justify="right"),
        Column(header="Age (s)", justify="center"),
        Column(header="Check Type", no_wrap=True),
        Column(header="Check Params", no_wrap=True),
        Column(header="Check Status"),
    ]

    get_column_index_by_header = lambda header_str: [
        col.header for col in columns
    ].index(header_str)

    columns_to_hide = []
    if not display_options["show_health_check_id"]:
        columns_to_hide.append(get_column_index_by_header("ID"))
    if not display_options["show_health_check_parameters"]:
        columns_to_hide.append(get_column_index_by_header("Check Params"))

    columns_to_hide = sorted(
        columns_to_hide, reverse=True
    )  # largest index first so popping one column doesn't affect index of others
    [columns.pop(i) for i in columns_to_hide]
    num_columns = len(columns)

    soh_table = Table(
        *columns,
        title="State of Health Check Results",
        expand=True,
    )

    check_id = 0
    prev_system = None
    for system in soh_results:
        if not prev_system:
            prev_system = system
        for check_data in soh_results[system]:
            tlm_point_value = (
                f"{round(check_data['tlm_value'], 3):.3f}"
                if isinstance(check_data["tlm_value"], (float, int))
                else check_data["tlm_value"]
            )

            age_colour = (
                "cyan" if check_data["check_status"] == CheckStatus.INCOMPLETE else ""
            )

            if check_data["check"]["check_type"] != "clock-latest":
                check_params = f"{check_data['check']['check_params']}"
            else:
                check_params = f"{check_data['check']['board']} clock"

            row_data = [
                f"{check_id:<2}",
                system,
                check_data["check"]["tlm_point"],
                tlm_point_value,
                Text(f"{check_data['tlm_age_seconds']:5g}", age_colour),
                check_data["check"]["check_type"],
                check_params,
                Text(
                    f"{check_data['check_status'].value}",
                    get_check_status_colour(check_data["check_status"]),
                ),
            ]

            [row_data.pop(i) for i in columns_to_hide]

            if prev_system != system:
                soh_table.add_row(*["" for _ in range(num_columns)])

            soh_table.add_row(*row_data)
            check_id += 1
            prev_system = system
    return soh_table


def get_FDIR_status_colour(status, score):
    """Get the colour string used to represent the given score and FDIR status.
    NOTE: To avoid visual clutter, white is returned when the score is zero for any status.

    Arguments:
        status (CheckStatus): FDIR status to which the aggregate score is attributed.
        score (int >= 0): Number of FDIR flags for the given subsystem which have the provided status.

    Returns:
        status_colour (str): Colour to be used when displaying the score for the given status. White is returned when
            the score is zero for any status.
    """
    FDIR_STATUS_COLOURS = {
        "NOT_MANAGED_OR_INACTIVE": "white",
        "OK": "green",
        "YELLOW": "bright_yellow",
        "RED": "bright_red",
        "NO_STATUS": "bright_red",
    }

    status_colour = FDIR_STATUS_COLOURS[status] if score != 0 else "white"

    return status_colour


def get_FDIR_results_table(
        fdir_results: dict, fdir_deltas: dict, display_options: dict
) -> Table:
    """Create a Table representing the current aggregated FDIR telemetry and any changes since the previous score check.

    Arguments:
        fdir_results (dict): Aggregated FDIR scores grouped by system and FDIR status. See calculate_FDIR_scores() for
            further details.
        fdir_deltas (dict): Score differences between previous and current FDIR results. See get_FDIR_score_deltas() for
            further details.
        display_options (dict, default = None): Printout display options - see main() for further details.

    Returns:
        fdir_table (rich.table.Table): Tabular representation of current aggregated FDIR telemetry and changes in these
            results since the last check was conducted.
    """
    # Organise data
    data = []  # data[i][j] is the text to display in row i, column j
    status_sums = {status: 0 for status in FDIRStatus.list()}

    for system in fdir_results:
        boards = ", ".join(fdir_results[system]["boards_required"])
        row_data = [system, boards]

        for status in FDIRStatus.list():
            current_count = fdir_results[system][status]["score"]
            count_delta = fdir_deltas[system][status]
            status_sums[status] += current_count

            delta_text = (
                ("(", (f"{count_delta:+}", "white on bright_red"), ")")
                if count_delta != 0
                else ("")
            )

            row_data.append(
                Text.assemble(
                    (
                        f"{current_count:<3d}",
                        get_FDIR_status_colour(status, current_count),
                    ),
                    *delta_text,  # type: ignore
                )
            )
        data.append(row_data)

    # Construct table
    fdir_table = Table(
        Column(header="System", footer="Sum"),
        Column(header="Boards", footer="-"),
        title="FDIR Flag Check Results",
        expand=True,
        show_footer=display_options["show_FDIR_status_sums"],
    )
    columns_indices_to_hide = []
    for i, status in enumerate(FDIRStatus.list()):
        status_sum = status_sums[status]
        column_should_be_hidden = (
                status in display_options["FDIR_hide_when_empty"] and status_sum == 0
        )

        if not column_should_be_hidden:
            fdir_table.add_column(
                header=status,
                footer=Text.assemble(
                    (f"{status_sum:<3d}", get_FDIR_status_colour(status, status_sum))
                ),
            )
        else:
            columns_indices_to_hide.append(2 + i)

    valid_indices = set(i for i in range(len(data[0]))).difference(
        columns_indices_to_hide
    )
    for row in data:
        reduced_row = [row[i] for i in valid_indices]
        fdir_table.add_row(*reduced_row)

    return fdir_table


def run_externally(display_options):
    """Run the autohealthcheck script externally as part of another script.

    Arguments:
        display_options (dict, default = None): Printout display options. Options include (WIP):
            'show_health_check_id': (bool) If true, show an ID for each health check result.
            'show_FDIR_transitions': (bool) If true, will show a printout of the status change for any flags that changed.
            'show_health_check_parameters': (bool) If true, show a column detailing parameters used for the health checks.
            'FDIR_hide_when_empty': (list of strings, matching possible FDIR statuses or "NO_STATUS") Statuses listed here
                will not be shown in the summary table if the scores for this status for all systems are zero.

    Returns: None.

    Creates: Autohealthcheck display in the console from which the command was run.
    """
    start_ext = datetime.utcnow()
    client = Client(verbose=False)
    console = Console()
    main(client, console, display_options)
    delta_ext = datetime.utcnow() - start_ext
    print(f"\nTime taken: {round(delta_ext.total_seconds(), 2):.2f} seconds")
    add_to_output_text(f"\nTime taken: {round(delta_ext.total_seconds(), 2):.2f} seconds")


def main(client: Client, console: Console, display_options: dict, quindar_client: QClient) -> None:
    """Conduct state of health check and FDIR change checks. This includes parsing check definitions, requesting telemetry,
    conducting the checks, and displaying check results to the user.

    Arguments:
        client (scepter.wrapper.Client instance): Scepter API client, used to query for telemetry.
        console (rich.console.Console instance): Console object, used for displaying results.
        display_options (dict, default = None): Printout display options. Options include (WIP):
            'show_health_check_id': (bool) If true, show an ID for each health check result.
            'show_FDIR_transitions': (bool) If true, will show a printout of the status change for any flags that changed.
            'show_health_check_parameters': (bool) If true, show a column detailing parameters used for the health checks.
            'FDIR_hide_when_empty': (list of strings, matching possible FDIR statuses or "NO_STATUS") Statuses listed here
                will not be shown in the summary table if the scores for this status for all systems are zero.

    Returns: None.
    """
    # Initialisation
    initialise_colorama()
    soh_checks = parse_SOH_config()
    fdir_checks = parse_FDIR_config()

    # Perform checks
    print(f"Beginning state of health checks...")
    add_to_output_text(f"Beginning state of health checks...")
    soh_telemetry = request_SOH_telemetry(client, soh_checks)
    soh_results = perform_SOH_checks(soh_checks, soh_telemetry)

    soh_table = get_SOH_results_table(soh_results, display_options)
    with open(SOH_FILE_NAME, 'w') as file:
        console = Console(file=file)
        console.print(soh_table)
    # console.print(soh_table)  # TODO: rework for live display

    # console.print()

    print("Checking FDIR flags...")
    add_to_output_text("Checking FDIR flags...")
    fdir_telemetry, fdir_packet_times = request_FDIR_telemetry(
        client, fdir_checks["metadata"]["primary_stack"]
    )
    if not check_FDIR_packet_times(fdir_checks, fdir_packet_times):
        contact_queue_client = ContactExecutionQueues(api_client=quindar_client)
        spacecraft_client = Spacecrafts(api_client=quindar_client)
        tickets_client = Tickets(api_client=quindar_client)
        filter = {
            'name': {'_eq': SPACECRAFT_NAME}
        }
        spacecraft = spacecraft_client.get(filter).response[0]
        contact_queue = contact_queue_client.get_queue(spacecraft_id=spacecraft.id)
        contact_queue.paused = True
        contact_queue_client.put(contact_queue.id, updated_resource=contact_queue)
        tickets_client.post(
            TicketCreate(
                summary=f"SOH FAILED on {SPACECRAFT_NAME}. Contact Queue paused! Check SOH!",
                tags=[SPACECRAFT_NAME, "SOH"],
                anomaly_timestamp=datetime.utcnow()
            )
        )
        #     summary: str
        #     description: Optional[str]
        #     status: TicketStatus
        #     type: TicketType
        #     priority: TicketPriority
        #     tags: Optional[List[str]]
        #     affected_services: Optional[List[str]]
        #     created_at: datetime
        #     updated_at: datetime
        #     anomaly_timestamp: Optional[datetime]
        #     comments: Optional[List[TicketComment]] = []
        # tickets_client.publish_event(
        #     SystemEvent(
        #         timestamp=datetime.utcnow(),
        #         source='ast-soh-check',
        #         event_type="CONTACT_QUEUE_PAUSED",
        #         tags=[SPACECRAFT_NAME, "SOH"],
        #         level="ERROR",
        #         message=f"SOH FAILED on {SPACECRAFT_NAME}. Contact Queue paused! Check SOH!"
        #     )
        # )

    fdir_data = process_FDIR_packets(fdir_telemetry)
    current_fdir_results = calculate_FDIR_scores(fdir_checks, fdir_data)
    prev_fdir_results = load_previous_FDIR_scores(quindar_client=quindar_client)
    fdir_deltas = get_FDIR_score_deltas(
        fdir_checks, prev_fdir_results, current_fdir_results
    )
    save_FDIR_scores(fdir_checks, current_fdir_results, fdir_packet_times, quindar_client)

    fdir_table = get_FDIR_results_table(
        current_fdir_results, fdir_deltas, display_options
    )
    with open(FDIR_FILE_NAME, 'w') as file:
        console = Console(file=file)
        console.print(fdir_table)  # TODO: rework to be live

    # Tell the operator which flags have changed
    if display_options and display_options["show_FDIR_transitions"]:
        with open(FLAG_TRANSITION_FILE_NAME, 'w') as file:
            console = Console(file=file)
            console.print(f"\nCounted {len(fdir_deltas['transitions'])} flag transitions:")
            add_to_output_text(f"\nCounted {len(fdir_deltas['transitions'])} flag transitions:")
            for flag in fdir_deltas["transitions"]:
                status_changed_from = fdir_deltas["transitions"][flag]["changed_from"]
                status_changed_to = fdir_deltas["transitions"][flag]["changed_to"]
                console.print(
                    Text.assemble(
                        (
                            f"\t{flag}: {status_changed_from} -> {status_changed_to}",
                            get_FDIR_status_colour(status_changed_to, 1),
                        )
                    )
                )
                add_to_output_text(Text.assemble(
                        (
                            f"\t{flag}: {status_changed_from} -> {status_changed_to}",
                            get_FDIR_status_colour(status_changed_to, 1),
                        )
                    ))

    with open(OUTPUT_MESSAGE_FILE_NAME, 'w') as file:
        file.write(output_message)



def add_to_output_text(message):
    global output_message
    output_message += f'{message} \n'
